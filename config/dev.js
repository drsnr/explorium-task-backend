module.exports = {
    "frontURL": "http://localhost:8080/",
    "OSMBaseURL": "http://www.overpass-api.de/api/xapi?*[amenity=*][bbox=",
    "env": {
        "name": "development",
        "isDev": true,
        "isProd": false
    }
}