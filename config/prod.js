module.exports = {
    "frontURL": process.env.FRONT_URL,
    "OSMBaseURL": process.env.OSM_BASE_URL,
    "env": {
        "name": "production",
        "isDev": false,
        "isProd": true
    }
}