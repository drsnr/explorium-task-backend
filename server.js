const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const history = require('connect-history-api-fallback');
const config = require('./config');

const enrichmentRoutes = require('./api/enrichment/enrichment.routes');

const app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(history({
    disableDotRule: true,
    verbose: true,
    htmlAcceptHeaders: ['text/html', 'application/xhtml+xml']
}));

if (config.env.isDev) {
    const corsOptions = {
        origin: [ 'http://10.0.0.8:8080', 'http://localhost:8080' ],
        credentials: true
    };
    app.use(cors(corsOptions));
} 

app.use(express.static(path.resolve(__dirname, 'public')));


app.use('/api/enrich', enrichmentRoutes);

app.get('/*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
    console.log('Server is running on port: ' + port);
});