const express = require('express');
const { enrichRow } = require('./enrichment.controller');
const router = express.Router();

router.post('/', enrichRow);

module.exports = router;