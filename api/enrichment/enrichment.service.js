const axios = require('axios');
const parseString = require('xml2js').parseString;
const config = require('../../config/index');

const enrichRow = (row) => {
    return new Promise(async (resolve, reject) => {
        try {
            const url = config.OSMBaseURL + _locationsToBoundingBox(+row.Lattitude, +row.Longtitude) + ']';
            const res = await axios.get(url, {
                headers: {
                    Referer: config.frontURL
                }
            });
            parseString(res.data, (err, result) => {
                const { node } = result.osm;
                row.schoolsCount = 0;
                if (node) {
                    row.schoolsCount = node.reduce((acc, { tag }) => {
                        if (!tag) return acc;
                        tag.forEach(({ $ }) => {
                            if ($.k === 'amenity' && $.v === 'school') acc++;
                        });
                        return acc;
                    }, 0);
                }
                resolve(row);
            });
        } catch (err) {
            reject(err);
        }
    });
}

const _locationsToBoundingBox = (latitude, longitude, padding = 0.01) => {
    const boundingBox = [ null, null, null, null ];
    boundingBox[0] = Math.min(boundingBox[0] || longitude, longitude) - padding;
    boundingBox[1] = Math.min(boundingBox[1] || latitude, latitude) - padding;
    boundingBox[2] = Math.max(boundingBox[2] || longitude, longitude) + padding;
    boundingBox[3] = Math.max(boundingBox[3] || latitude, latitude) + padding;
    return boundingBox;
}

module.exports = {
    enrichRow
}
