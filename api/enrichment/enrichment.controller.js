const enrichmentService = require('./enrichment.service');

const enrichRow = async (req, res) => {
    const row = req.body;
    try {
        const enrichedRow = await enrichmentService.enrichRow(row);
        res.json(enrichedRow);
    } catch (err) {
        res.status(429).send({ err });
    }
}

module.exports = {
    enrichRow
}